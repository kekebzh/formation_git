\chapter{Théorie de Git}

\section{Différences et Versionning}

Manipuler des différences entre 2 fichiers de type texte est une opération très courante.

Dans un environnement de programmation, on fait appel à une commande \texttt{diff} pour cela.

Par exemple, la commande diff entre 2 fichers donne :

\begin{diffcode}
$ diff -u collection.rb collection2.rb 
--- collection.rb	2015-07-24 14:15:07.416308012 +0200
+++ collection2.rb	2015-08-17 11:45:53.305468727 +0200
@@ -7,7 +7,6 @@
 #  unit_id
 class Collection < ActiveRecord::Base
   belongs_to :station
-  belongs_to :unit
+  has_many :units
   belongs_to :samplemethod
   has_many :samples
   has_many :results

\end{diffcode} 
%$

C'est de cette manière que les gestionnaires de version peuvent enregistrer et garder un historique des versions successives d'un projet de développement.

\begin{Exercise}[title={Travailler avec la commande diff},name=Exercice]

Dans le dossier TP, prenez le fichier d'exemple "exercice1.txt"

Faites une copie de ce fichier et renommez-la "exercice1\_mine.txt"

Insérez des modifications à plusieurs endroits, et testez la sortie des commandes suivantes :

\begin{consolecode}
$ diff -u exercice1.txt exercice1_mine.txt
$ diff -u exercice1_mine.txt exercice1.txt
\end{consolecode}

\end{Exercise}

\section{Fonctionnement de Git}

\subsection{Un dépôt}

Un dépôt git est un répertoire (ou dossier) dans lequel toute modification peut être suivie dans le temps.

On peut initialiser un dépôt git dans n'importe quel répertoire. Tous les sous-répertoires feront partie du dépôt.

Lorsqu'on appelle le programme git, il cherche à savoir s'il se trouve dans un dépôt ou non. Pour cela, il cherche la présence d'un sous-répertoire caché \texttt{.git/} en remontant l'arborescence.

\subsection{Représentation interne du dépôt}

Dans un dépôt git, tout fichier est dans 3 états possibles. On peut faire un bilan de l'état de chaque fichier dans un dépôt avec la commande \texttt{status} :

\mint{console}|git status|

Les trois états possibles sont les suivants : 

\begin{description}
\item[modifié] Un fichier dans cet état a été localement modifié, mais git n'a pas validé les différences dans sa base de données locale. Les fichiers dans cet état sont dans le répertoire de travail (ou \texttt{working directory}).
\item[indexé] Un fichier dans cet état fait partie des modifications qui sont prêtes à être validées pour le prochain instantané. Les fichiers dans cet état sont dans l'index (ou dans le \texttt{staging area}).
\item[validé] Les données dans cet état sont en sécurité dans la base de donnée locale de git (aussi appelée \texttt{git directory}). Les modification ainsi sauvegardées font partie d'un instantané du projet (appelé aussi \texttt{commit}).
\end{description}

Le schéma \ref{fig:3etats} représente ces 3 états et les opérations de transition.

De manière classique, on utilise git en suivant les étapes suivante :

\begin{enumerate}
\item On modifie un fichier localement
\item On ajoute la modification à l'index (\textbf{staging})
\item On enregistre un instantané du projet à partir de l'index (\textbf{commit}) 
\end{enumerate}

\begin{figure}
\begin{center}
\includegraphics[width=.7\linewidth]{images/3etats}
\end{center}
\caption{Les 3 états dans un dépôt Git et leurs transitions}
\label{fig:3etats}
\end{figure}


Ces notions sont fondamentales dans git et il est primordial de bien les comprendre.

\subsubsection*{Ajout à l'index}

Ajouter une modification à l'index se fait par la commande \texttt{add}.

Après avoir modifié un fichier ou créé un nouveau fichier, on place ces changements dans l'index : 

\mint{console}|git add mon_fichier|

L'ensemble des modifications de l'index permettent de maintenant d'enregistrer un instantané.

\subsubsection*{Enregistrement d'un instantané}

Enregistrer un instantané, c'est écrire dans la base de donnée locale de git les modifications indexées. La commande git pour cette opération est \texttt{commit}.

\mint{console}|git commit|

Nous détaillerons cette opération dans la section \ref{sec:commit}.

\section{Les instantanés du projet}
\label{sec:commit}

Git enregistre dans une base de donnée locale une succession d’instantanés (ou \texttt{commits}).

La figure \ref{fig:snapshots} illustre cette succession dans le temps.

\begin{figure}[h]
\begin{center}
\includegraphics[width=.7\linewidth]{images/checkins}
\caption{Les instantanés dans le temps reconstituent l'ensemble des fichiers du projet à un instant}
\label{fig:snapshots}
\end{center}

\end{figure}

Un instantané a plusieurs caractéristiques fondamentales :

\begin{description}
\item[unicité] Un instantané est unique et identifié par un numéro de série (par exemple \texttt{f31a288e90dae7712f49b061a56486f6d489a657}
\item[auteur] Un instantané est lié à un auteur (nom + adresse mail)
\item[date] Un instantané est horodaté
\item[commentaire] Un instantané est obligatoirement accompagné d'un commentaire de son auteur.
\end{description}

\subsection{lister les instantanés}

La commande \texttt{log} permet de lister tous les instantanés successifs du projet :
\mint{console}|git log|

\subsection{Créer un instantané}

La commande \texttt{commit} permet de créer un instantané à partir de l'index :

\mint{console}|git commit|

\subsection{Commenter un instantané}

La commande \texttt{commit} ouvre une fenêtre d'édition dans laquelle l'auteur va écrire le commentaire attaché à son commit.

Voici les règles courantes implicites d'un bon message de commit :

\begin{enumerate}
\item Un titre (maximum 80 caractères)
\item Un saut de ligne
\item Des détails sur les modifications de ce commit
\end{enumerate}

L'auteur, la date et l'identifiant du commit sont automatiquement ajoutés par git.

\subsection{Configuration pour les commits}
\label{sec:config}
On peut configurer globalement un auteur :

\begin{consolecode}
$ git config --global user.name "Gaston Lagaffe"
$ git config --global user.mail "gaston.lagaffe@journal-de-spirou.fr"
\end{consolecode}

On peut configurer l'éditeur que git vous présentera pour rédiger les messages de commit (ici, je propose vim, mais on peut aussi choisi d'autres alternatives comme nano) :

\begin{consolecode}
$ git config --global core.editor nano 

\end{consolecode} 
%$

Pour informations, ces configurations sont stockées dans un simple fichier : \texttt{~/.gitconfig}.

\begin{Exercise}[title={Un premier commit},name=Exercice,label=exo:commit]

Avant tout, nous allons configurer Git comme indiqué dans la section \ref{sec:config}.

Maintenant, nous allons nous intéresser à nouveau à la création de nouveaux commits.

Commençons par examiner le journal de notre dépôt :

\mint{console}|git log|

Ensuite, vérifions l'état de notre dépôt :

\begin{consolecode}
$ git status
Sur la branche master
rien à valider, la copie de travail est propre
\end{consolecode}
%$
Créez un nouveau fichier README dans le dépôt, écrivez un messagee à l'intérieur et ajoutez ce fichier dans l'index.

Ensuite, créez un commit, en respectant les règles énoncées plus haut pour le message.

\begin{consolecode}
$ git commit
[master (commit racine) bf67c41] Titre court
 1 file changed, 1 insertion(+)
 create mode 100644 mon_fichier.txt
\end{consolecode}
%$
Notez les informations renvoyées par la commande commit, en particulier :

\mint{console}|create mode 100644 mon_fichier.txt|

Ceci indique que git a enregistré la création d'un nouveau fichier.

\begin{consolecode}
$ git log
commit 2e510ae71d021486cc86ec744c0320c2ad665cca
Author: Jonathan Schaeffer <schaeffer@univ-brest.fr>
Date:   Thu Aug 27 16:06:54 2015 +0200
    Ajout d'un fichier README
    
    Ce sera la documentation du projet

commit a2fa537bda968d92ac98d9813157c2e44e021126
Author: Jonathan Schaeffer <schaeffer@univ-brest.fr>
Date:   Mon Aug 24 16:31:44 2015 +0200
    Première validation
\end{consolecode}
%$
Maintenant, le but de l'exercice est de créer deux nouveaux commits :

\begin{enumerate}
\item modification du fichier README existant
\item ajout d'un nouveau fichier (c'est à vous de choisir un nom cette fois-ci)
\end{enumerate}

Pensez à toujours ajouter à l'index les modifications que vous allez valider.

Nous allons voir comment supprimer un fichier de notre dépôt.

\begin{enumerate}
\item Supprimez le fichier \texttt{salut.rb\textasciitilde}
\item Examinez l'état du dépôt ainsi que son contenu. Que constatez-vous ?
\item Récupérez le fichier supprimé en suivant l'indication donnée par la commande \texttt{git status}
\item Vérifiez l'état du dépôt ainsi que son contenu. Que constatez-vous ?
\item Utilisez la commande \texttt{git rm} afin de supprimer ce fichier
\item Examinez l'état du dépôt ainsi que son contenu. Que constatez-vous ?
\item Enregistrez un instantané de votre dépôt. Indiquez un message bien explicite.
\item Examinez l'état du dépôt ainsi que son contenu. Que constatez-vous ?
\end{enumerate}

\end{Exercise}

\begin{Answer}[ref=exo:commit]
Pour supprimer un fichier dans un dépôt git, on utilise la commande \texttt{git rm}.

Par exemple :

\begin{consolecode}
$ git rm mon_fichier.txt
$ git commit
[master 2f051e6] Suppression du fichier
 1 file changed, 1 deletion(-)
 delete mode 100644 mon_fichier.txt
\end{consolecode}

On aura la possibilité d'annuler cette suppression comme on le verra dans la section \ref{sec:revert}

La séquence de suppression demandée dans l'exercice est la suivante :

\begin{enumerate}
\item \mint{console}| rm salut.rb~|
\item Le fichier est supprimé, mais il y a des modifications à indexer.
\item \mint{console}| git checkout -- salut.rb~|
\item le fichier est à nouveau présent
\item \mint{console}| git rm salut.rb~|
\item L'index est prêt pour un instantanné
\item \mint{console}| git commit -m "Suppression du fichier salut.rb~"|
\item Le dépôt est dans un état propre.
\end{enumerate}

\end{Answer}

\subsection{Choisir les fichier à ignorer}

Dans notre projet, il reste un fichier dont le nom termine par un "\textasciitilde". Cela indique un fichier utilisé par l'éditeur de texte comme copie temporaire. Chaque fois qu'on édite un fichier, ce genre de fichier temporaire est créé. On ne veut pas que ce type de fichiers soit suivis par git.

On peut préciser une liste de fichiers que git va ignorer. Cette liste est dans un ficher \texttt{.gitignore}, chaque ligne de ce fichier indique un modèle de nom de fichier à ignorer.

Ainsi, si on veut que git ignore :

\begin{itemize}
\item tous les fichiers finissant par "\textasciitilde", 
\item tous les fichiers d'un sous-répertoire log
\end{itemize}

on va créer le fichier \texttt{.gitignore} afin qu'il contienne :

\begin{consolecode}
*~
log/*
\end{consolecode}

Les fichiers correspondant à cette description seront ignorés. Attention, s'ils sont déjà dans le versionning, ils continueront d'être suivis.


\begin{Exercise}[title={Git Ignore},label=exo:ignore,name=Exercice]

Nous allons dire à git d'ignorer les fichier finissant par "\textasciitilde" et les fichiers d'un répertoire log.

Commencez par créer un répertoire \texttt{log} ainsi qu'un fichier dans ce répertoire, et vérifiez que git s'en est bien rendu compte :

\begin{consolecode}
$ mkdir log
$ touch log/exemple.log
$ git status
Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
    log/
\end{consolecode}
%$
Créer un fichier \texttt{.gitignore} avec comme contenu :

\begin{consolecode}
log/*
\end{consolecode}

Puis voyez comment git voit le dépôt :

\begin{consolecode}
$ git status
Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
    .gitignore
\end{consolecode}
%$
Le répertoire log n'est plus vu par git, il l'ignore !

Maintenant, pour les fichiers terminant par un "\textasciitilde", procédons en 2 étapes :

D'abord retirer les fichiers du versionning : 

\begin{consolecode}
$ git rm --cached *~
\end{consolecode}
%$
Cette étape nous laisse avec les fichiers terminant par "\textasciitilde" toujours présents dans le répertoire de travail, mais git les a supprimés du versionning.

Maintenant, pour les ignorer à tout jamais, ajouter la ligne correspondante dans \texttt{.gitignore} :

\begin{consolecode}
*~
\end{consolecode}

On termine en ajoutant \texttt{.gitignore} dans le suivi de git et en faisant un commit.
\end{Exercise}


\section{Conclusion}

Nous savons maintenant :
\begin{itemize}
\item comment fonctionne un dépôt git
\item comment faire un instantané de notre projet
\item consulter l'état d'un dépôt git
\end{itemize}

Nous allons maintenant voir comment manipuler les instantanés pour remonter l'histoire.